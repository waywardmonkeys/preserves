# Preserves Schema for TypeScript/JavaScript

This is an implementation of [Preserves Schema](https://preserves.dev/preserves-schema.html)
for TypeScript and JavaScript.
