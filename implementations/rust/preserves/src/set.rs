use crate::value::{self, to_value, IOValue, UnwrappedIOValue};
use std::iter::IntoIterator;
use serde::{Serialize, Serializer, Deserialize, Deserializer};

pub fn serialize<S, T, Item>(s: T, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
    T: IntoIterator<Item = Item>,
    Item: Serialize,
{
    let s = s.into_iter().map(to_value).collect::<value::Set<IOValue>>();
    UnwrappedIOValue::from(s).wrap().serialize(serializer)
}

pub fn deserialize<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
    T: Deserialize<'de>,
{
    // Relies on the way we hack around serde's data model in de.rs and value/de.rs.
    T::deserialize(deserializer)
}
