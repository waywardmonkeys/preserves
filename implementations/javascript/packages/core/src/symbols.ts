// Symbols for various Preserves protocols.

// Previously, we had the following:
//
// export const PreserveOn = Symbol.for('PreserveOn');
// export const AsPreserve = Symbol.for('AsPreserve');
// export const FromPreserve = Symbol.for('FromPreserve');
//
// ... and used them to define methods [PreserveOn] and [AsPreserve]
// and functions [FromPreserve] in various namespaces. The methods
// worked well, but the functions didn't work with TypeScript as of TS
// 4.5 because there's no way to export a symbol-named property from a
// `namespace`. See
// https://github.com/microsoft/TypeScript/issues/36813.
//
// So, instead, I've converted them as follows:
//
//   [PreserveOn] ==> __preserve_on__
//   [AsPreserve] ==> __as_preserve__
//   [FromPreserve] ==> __from_preserve__
//
// and emphasised use of functions `fromJS` and `toJS` to get away
// from the `__fiddly_details__`.
