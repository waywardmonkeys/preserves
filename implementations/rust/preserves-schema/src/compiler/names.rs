use convert_case::{Case, Casing};

pub fn render_constructor(n: &str) -> String {
    n.to_case(Case::UpperCamel)
}

pub fn render_fieldname(n: &str) -> String {
    n.to_case(Case::Snake)
}

pub fn render_modname(n: &str) -> String {
    n.to_case(Case::Snake)
}
