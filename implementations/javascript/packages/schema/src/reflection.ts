import type { Value } from '@preserves/core';

export type Imports = { [modulePath: string]: any }; // TODO: better than any

export interface SchemaDefinition {
    schema: Value,
    imports: Imports,
    definitionName: symbol,
    variant?: symbol,
}
