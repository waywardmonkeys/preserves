{ Type = ./Type.dhall
, function = ./function.dhall
, boolean = ./boolean.dhall
, dictionary = ./dictionary.dhall
, dictionaryOf = ./dictionaryOf.dhall
, double = ./double.dhall
, embedded = ./embedded.dhall
, fromJSON = ./fromJSON.dhall
, integer = ./integer.dhall
, record = ./record.dhall
, render = ./render.dhall
, sequence = ./sequence.dhall
, sequenceOf = ./sequenceOf.dhall
, string = ./string.dhall
, symbol = ./symbol.dhall
}
