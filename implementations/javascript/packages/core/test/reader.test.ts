import { Bytes, Decoder, genericEmbeddedType, encode, Reader, Double } from '../src/index';
import './test-utils';

import * as fs from 'fs';

describe('reading common test suite', () => {
    const samples_bin = fs.readFileSync(__dirname + '/../../../../../tests/samples.bin');
    const samples_pr = fs.readFileSync(__dirname + '/../../../../../tests/samples.pr', 'utf-8');

    it('should read equal to decoded binary without annotations', () => {
        const s1 = new Reader(samples_pr, { embeddedDecode: genericEmbeddedType, includeAnnotations: false }).next();
        const s2 = new Decoder(samples_bin, { embeddedDecode: genericEmbeddedType, includeAnnotations: false }).next();
        expect(s1).is(s2);
    });

    it('should read equal to decoded binary with annotations', () => {
        const s1 = new Reader(samples_pr, { embeddedDecode: genericEmbeddedType, includeAnnotations: true }).next();
        const s2 = new Decoder(samples_bin, { embeddedDecode: genericEmbeddedType, includeAnnotations: true }).next();
        expect(s1).is(s2);
    });

    it('should read and encode back to binary with annotations', () => {
        const s = new Reader(samples_pr, { embeddedDecode: genericEmbeddedType, includeAnnotations: true }).next();
        const bs = Bytes.toIO(encode(s, {
            embeddedEncode: genericEmbeddedType,
            includeAnnotations: true,
            canonical: true,
        }));
        // console.log('original', new Bytes(samples_bin).toHex());
        // console.log('reencoded', new Bytes(bs).toHex());
        expect(bs).toEqual(new Uint8Array(samples_bin));
    });

    it('should be ok with no whitespace at the end of a numeric input', () => {
        expect(new Reader('123').next()).toEqual(123);
        expect(new Reader('123.0').next()).toEqual(Double(123.0));
        expect(new Reader('123.00').next()).toEqual(Double(123.0));
    });
});
