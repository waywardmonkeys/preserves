use crate::value::{IOValue, NestedValue};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Symbol(pub String);

impl serde::Serialize for Symbol {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: serde::Serializer {
        IOValue::symbol(&self.0).serialize(serializer)
    }
}

impl<'de> serde::Deserialize<'de> for Symbol {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: serde::Deserializer<'de> {
        let v = IOValue::deserialize(deserializer)?;
        let s = v.value().as_symbol().ok_or_else(|| serde::de::Error::custom("Expected symbol"))?;
        Ok(Symbol(s.clone()))
    }
}

pub fn serialize<S>(s: &str, serializer: S) -> Result<S::Ok, S::Error> where S: serde::Serializer {
    use serde::Serialize;
    Symbol(s.to_string()).serialize(serializer)
}

pub fn deserialize<'de, D>(deserializer: D) ->
    Result<String, D::Error> where D: serde::Deserializer<'de>
{
    use serde::Deserialize;
    Symbol::deserialize(deserializer).map(|v| v.0)
}
