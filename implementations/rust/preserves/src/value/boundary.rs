#[derive(Default, Clone, Debug)]
pub struct Type {
    pub closing: Option<Item>,
    pub opening: Option<Item>,
}

#[derive(Clone, Debug)]
pub enum Item {
    Annotation,
    AnnotatedValue,
    DictionaryKey,
    DictionaryValue,
    RecordField,
    RecordLabel,
    SequenceValue,
    SetValue,
}

impl Type {
    #[inline]
    pub fn shift(&mut self, i: Option<Item>) {
        let tmp = std::mem::replace(&mut self.opening, i);
        self.closing = tmp;
    }
}

pub fn start(i: Item) -> Type {
    Type {
        closing: None,
        opening: Some(i),
    }
}

pub fn mid(c: Item, o: Item) -> Type {
    Type {
        closing: Some(c),
        opening: Some(o),
    }
}

pub fn end(i: Item) -> Type {
    Type {
        closing: Some(i),
        opening: None,
    }
}
