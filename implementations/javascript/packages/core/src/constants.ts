export enum Tag {
    False = 0x80,
    True,
    Float,
    Double,
    End,
    Annotation,
    Embedded,

    SmallInteger_lo = 0x90,
    MediumInteger_lo = 0xa0,

    SignedInteger = 0xb0,
    String,
    ByteString,
    Symbol,
    Record,
    Sequence,
    Set,
    Dictionary,
}
