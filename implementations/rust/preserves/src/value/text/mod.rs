pub mod reader;
pub mod writer;

pub use reader::TextReader;
pub use writer::TextWriter;

use crate::value::reader::BytesBinarySource;

use std::io;

use super::{DomainParse, IOValue, IOValueDomainCodec, NestedValue, Reader, ViaCodec};

pub fn from_str<N: NestedValue, Dec: DomainParse<N::Embedded>>(
    s: &str,
    decode_embedded: Dec,
) -> io::Result<N> {
    TextReader::new(&mut BytesBinarySource::new(s.as_bytes()), decode_embedded).demand_next(false)
}

pub fn iovalue_from_str(s: &str) -> io::Result<IOValue> {
    from_str(s, ViaCodec::new(IOValueDomainCodec))
}

pub fn annotated_from_str<N: NestedValue, Dec: DomainParse<N::Embedded>>(
    s: &str,
    decode_embedded: Dec,
) -> io::Result<N> {
    TextReader::new(&mut BytesBinarySource::new(s.as_bytes()), decode_embedded).demand_next(true)
}

pub fn annotated_iovalue_from_str(s: &str) -> io::Result<IOValue> {
    annotated_from_str(s, ViaCodec::new(IOValueDomainCodec))
}
