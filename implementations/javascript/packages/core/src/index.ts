export * from './runtime';
export * as Constants from './constants';

import type { Value } from './values';

declare global {
    interface ArrayConstructor {
        __from_preserve__<T>(v: Value<T>): undefined | Array<Value<T>>;
    }
}

Array.__from_preserve__ = <T>(v: Value<T>) => {
    return Array.isArray(v) ? v : void 0;
};

const _Array = Array;
type _Array<T> = Array<T>;
export { _Array as Array };

const _Symbol = Symbol;
type _Symbol = Symbol;
export { _Symbol as Symbol };
