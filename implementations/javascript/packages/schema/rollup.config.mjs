import terser from '@rollup/plugin-terser';

const distfile = (insertion) => `dist/preserves-schema${insertion}.js`;

function umd(insertion, extra) {
  return {
    file: distfile(insertion),
    format: 'umd',
    name: 'PreservesSchema',
    globals: {
      '@preserves/core': 'Preserves',
    },
    ... (extra || {})
  };
}

function es6(insertion, extra) {
  return {
    file: distfile('.es6' + insertion),
    format: 'es',
    globals: {
      '@preserves/core': 'Preserves',
    },
    ... (extra || {}),
  };
}

function cli(name) {
  return {
    input: `lib/bin/${name}.js`,
    output: [{file: `dist/bin/${name}.js`, format: 'commonjs'}],
    external: [
      '@preserves/core',
      'chalk',
      'chokidar',
      'fs',
      'glob',
      'minimatch',
      'path',
      'commander',
    ],
  };
}

export default [
  {
    input: 'lib/index.js',
    output: [
      umd(''),
      umd('.min', { plugins: [terser()] }),
      es6(''),
      es6('.min', { plugins: [terser()] }),
    ],
    external: ['@preserves/core'],
  },
  cli('preserves-schema-ts'),
  cli('preserves-schemac'),
];
