{-|
Create a Preserves integer value from an `Integer` value
-}
let Preserves = ./Type.dhall

let Preserves/function = ./function.dhall

let integer
    : Integer → Preserves
    = λ(x : Integer) →
      λ(Preserves : Type) →
      λ(value : Preserves/function Preserves) →
        value.integer x

in  integer
