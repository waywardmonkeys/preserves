// Patching to support node.js extensions.

import { Annotated } from './annotated';
import { Bytes } from './bytes';
import { Set, Dictionary } from './dictionary';
import { stringify } from './text';

import * as util from 'util';

[Bytes, Annotated, Set, Dictionary].forEach((C) => {
    (C as any).prototype[util.inspect.custom] =
        function (_depth: any, _options: any) {
            return stringify(this, { indent: 2 });
        };
});
