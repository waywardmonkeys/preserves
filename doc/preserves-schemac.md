---
title: preserves-schemac
---

The `preserves-schemac` program reads
[Preserves Schema](../preserves-schema.html) DSL input files and
outputs a binary-syntax Preserves document conforming to the
[metaschema](https://gitlab.com/preserves/preserves/-/blob/main/schema/schema.prs).

It can either output single `Schema` records (corresponding to a
single input file), or a `Bundle` of `Schema`s (corresponding to a
directory tree of files).

## Installation

Install node.js v12 or newer. Then, `yarn global add @preserves/schema`.

## Usage

    Usage: preserves-schemac [options] [input...]

    Compile textual Preserves schema definitions to binary format

    Arguments:
      input        Input directory, with optional ":glob" appended (defaults to ":**/*.prs")

    Options:
      --no-bundle  Emit a single Schema instead of a schema Bundle
      -h, --help   display help for command

## Examples

### Single file (non-bundle)

Given a file [`demo.prs`](demo.prs) containing:

    version 1 .
    JSON =
         / @string string
         / @integer int
         / @double double
         / @boolean JSONBoolean
         / @null =null
         / @array [JSON ...]
         / @object { string: JSON ...:... } .
    JSONBoolean = =true / =false .

running the following:

    preserves-schemac --no-bundle .:demo.prs

will produce the following binary file on `stdout`:

    00000000: b4b3 0673 6368 656d 61b7 b307 7665 7273  ...schema...vers
    00000010: 696f 6e91 b30b 6465 6669 6e69 7469 6f6e  ion...definition
    00000020: 73b7 b304 4a53 4f4e b4b3 026f 72b5 b5b1  s...JSON...or...
    00000030: 0673 7472 696e 67b4 b304 6174 6f6d b306  .string...atom..
    00000040: 5374 7269 6e67 8484 b5b1 0769 6e74 6567  String.....integ
    00000050: 6572 b4b3 0461 746f 6db3 0d53 6967 6e65  er...atom..Signe
    00000060: 6449 6e74 6567 6572 8484 b5b1 0664 6f75  dInteger.....dou
    00000070: 626c 65b4 b304 6174 6f6d b306 446f 7562  ble...atom..Doub
    00000080: 6c65 8484 b5b1 0762 6f6f 6c65 616e b4b3  le.....boolean..
    00000090: 0372 6566 b584 b30b 4a53 4f4e 426f 6f6c  .ref....JSONBool
    000000a0: 6561 6e84 84b5 b104 6e75 6c6c b4b3 036c  ean.....null...l
    000000b0: 6974 b304 6e75 6c6c 8484 b5b1 0561 7272  it..null.....arr
    000000c0: 6179 b4b3 0573 6571 6f66 b4b3 0372 6566  ay...seqof...ref
    000000d0: b584 b304 4a53 4f4e 8484 84b5 b106 6f62  ....JSON......ob
    000000e0: 6a65 6374 b4b3 0664 6963 746f 66b4 b304  ject...dictof...
    000000f0: 6174 6f6d b306 5374 7269 6e67 84b4 b303  atom..String....
    00000100: 7265 66b5 84b3 044a 534f 4e84 8484 8484  ref....JSON.....
    00000110: b30b 4a53 4f4e 426f 6f6c 6561 6eb4 b302  ..JSONBoolean...
    00000120: 6f72 b5b5 b104 7472 7565 b4b3 036c 6974  or....true...lit
    00000130: b304 7472 7565 8484 b5b1 0566 616c 7365  ..true.....false
    00000140: b4b3 036c 6974 b305 6661 6c73 6584 8484  ...lit..false...
    00000150: 8484 b30c 656d 6265 6464 6564 5479 7065  ....embeddedType
    00000160: 8084 84                                  ...

Piping the output to [`preserves-tool`](./preserves-tool.html) to
pretty-print it produces:

    <schema {
      version: 1,
      embeddedType: #f,
      definitions: {
        JSONBoolean: <or [
          [
            "true",
            <lit true>
          ],
          [
            "false",
            <lit false>
          ]
        ]>,
        JSON: <or [
          [
            "string",
            <atom String>
          ],
          [
            "integer",
            <atom SignedInteger>
          ],
          [
            "double",
            <atom Double>
          ],
          [
            "boolean",
            <ref [] JSONBoolean>
          ],
          [
            "null",
            <lit null>
          ],
          [
            "array",
            <seqof <ref [] JSON>>
          ],
          [
            "object",
            <dictof <atom String> <ref [] JSON>>
          ]
        ]>
      }
    }>

### Multiple file (bundle)

Given a directory tree containing multiple `*.prs` files, running

    preserves-schemac .

will produce a binary `Bundle` on `stdout` containing one `Schema` for
each input file in the tree.
