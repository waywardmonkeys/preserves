Python's strings, byte strings, integers, booleans, and double-precision floats stand directly
for their Preserves counterparts. Wrapper objects for [Float][preserves.values.Float] and
[Symbol][preserves.values.Symbol] complete the suite of atomic types.

Python's lists and tuples correspond to Preserves `Sequence`s, and dicts and sets to
`Dictionary` and `Set` values, respectively. Preserves `Record`s are represented by
[Record][preserves.values.Record] objects. Finally, embedded values are represented by
[Embedded][preserves.values.Embedded] objects.
