use preserves::symbol::Symbol;
use preserves::value::{IOValue, Map};

#[derive(Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct TestCases {
    pub tests: Map<Symbol, TestCase>
}

#[derive(Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum TestCase {
    Test(#[serde(with = "serde_bytes")] Vec<u8>, IOValue),
    NondeterministicTest(#[serde(with = "serde_bytes")] Vec<u8>, IOValue),
    DecodeTest(#[serde(with = "serde_bytes")] Vec<u8>, IOValue),
    ParseError(String),
    ParseShort(String),
    ParseEOF(String),
    DecodeError(#[serde(with = "serde_bytes")] Vec<u8>),
    DecodeShort(#[serde(with = "serde_bytes")] Vec<u8>),
    DecodeEOF(#[serde(with = "serde_bytes")] Vec<u8>),
}
