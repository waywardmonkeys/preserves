import { Single, Double, fromJS, Dictionary, IDENTITY_FOLD, fold, mapEmbeddeds, Value, embed } from '../src/index';
import './test-utils';

describe('Single', () => {
    it('should print reasonably', () => {
        expect(Single(123.45).toString()).toEqual("123.45f");
  });
});

describe('Double', () => {
    it('should print reasonably', () => {
        expect(Double(123.45).toString()).toEqual("123.45");
  });
});

describe('fold', () => {
    function mkv<T extends object>(t: T): Value<T> {
        return fromJS<T>([
            1,
            2,
            new Dictionary([[[3, 4], fromJS([5, 6])],
                            ['a', 1],
                            ['b', true]]),
            Single(3.4),
            t,
        ]);
    }

    it('should support identity', () => {
        const w = new Date();
        const v = mkv(w);
        expect(fold(v, IDENTITY_FOLD)).is(v);
        const w1 = new Date();
        const v1 = mkv(w1);
        expect(fold(v, IDENTITY_FOLD)).not.is(v1);
        expect(mapEmbeddeds(v, _t => embed(w1))).is(v1);
    });
});

describe('fromJS', () => {
    it('should map integers to themselves', () => {
        expect(fromJS(1)).toBe(1);
    });
});
